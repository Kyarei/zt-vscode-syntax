# Change Log

## 0.0.4

- Fix highlighting in general

## 0.0.3
- Fix comment highlighting

## 0.0.1
- Initial release