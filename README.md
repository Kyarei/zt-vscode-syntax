# zt--vscode-syntax

A syntax highlighter for [ztš](https://gitlab.com/Kyarei/zt).

## Release Notes

### 0.0.3

Fix comment highlighting.

### 0.0.1

Initial release of ztš-vscode-syntax.
